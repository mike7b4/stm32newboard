# V1.1 (In progress)

 - Fix USBD+/USBD- so they are both same length.
 - Fix LED Resistor track.
 - Fix MCP16311 VIN track size.

# V1.0

 - Sent to JLPCB fabrication


