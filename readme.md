# STM32newboard

## Powering
 **Note! VIN is labeled 12v in schematic but can be between 6-15 V.
 For example: 2 to 4 LiFe4Po 18650 cells in serial may work.**

 - Polarity protection FET SI2319
 - Switch regulator MCP16311
   - Voltage input: 6-15 V
   - Voltage out: 5 V
 - Low dropout regulator MCP1702
   - Voltage input: 5 V via MCP16311
   - Voltage output: 3.3 V
 - Dual FET FDS6930 see below.

## MCU STM32F042F6 SOP:

 **Note! Since I decided to not put a external crystal for the STM32 one may not use this card in "unstable" temperatures etc.
 I may decide to add a crystral in nextgen but since this is the first revision it probably has other bugs too fix also.**

  - STM32 Has:
    - Flash 32Kb. 6 Kb RAM in internal 48 Mhz "stable" oscillator.
 - External Peripherals connected:
   - Optional LED. 
   - Optional I2C EEPROM
   - Optional dual FDS6930 FET
     - 1 channel connected to 5v (via BT pin)
     - 1 channel connected to VIN and wired to LOAD1 terminal output.
   - Optional USB **or** UART1 (connected to HCx BT module.)
   - Optional UART2 connected to an external RS485 modoule.
   - Analog measure of VIN.

# Need to get verified:


 - [x] SI2319 No voltage on VIN if inverse polarity.
 - [x] MCP16311 delivers stable 5Vout
 - [x] MCP1702 delivers stable 3.3Vout
 - [x] MCU reset/powering
 - [x] USB
 - [ ] EEPROM correct resistors etc...
 - [ ] FDS
 - [ ] LED
 - [ ] Analog measure

# Next gen fixes needed

 - [ ] Add crystal on PFx (EEPROM has to be removed or use SW bitbang on other PIN's)
 - [ ] Add ESP protection on USB
 - [ ] Use another cheaper FET than FDS?
